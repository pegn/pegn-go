package is

var Digit = digit{}

type digit struct{}

func (digit) Ident() string     { return "digit" }
func (digit) PEGN() string      { return "[0-9]" }
func (digit) Desc() string      { return `ASCII digit 0 through 9` }
func (digit) Check(r rune) bool { return '0' <= r && r <= '9' }
