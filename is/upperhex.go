package is

var Upperhex = upperhex{}

type upperhex struct{}

func (upperhex) Ident() string { return "upperhex" }
func (upperhex) PEGN() string  { return "[0-9] / [A-F]" }
func (upperhex) Desc() string {
	return `digit 0 through 0 or uppercase letter A through F`
}
func (upperhex) Check(r rune) bool {
	return Digit.Check(r) || 'A' <= r && r <= 'F'
}
