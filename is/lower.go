package is

var Lower = lower{}

type lower struct{}

func (lower) Ident() string     { return "lower" }
func (lower) PEGN() string      { return "[a-z]" }
func (lower) Desc() string      { return `any ASCII lowercase letter` }
func (lower) Check(r rune) bool { return 'a' <= r && r <= 'z' }
