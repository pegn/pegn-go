package is

var Sign = sign{}

type sign struct{}

func (sign) Ident() string     { return "sign" }
func (sign) PEGN() string      { return "PLUS / MINUS" }
func (sign) Desc() string      { return `plus + or minus +` }
func (sign) Check(r rune) bool { return r == '+' || r == '-' }
