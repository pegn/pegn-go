package main

// TODO maybe make the following a reality some day
///go:generate mainmaker

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
)

func main() {
	args := os.Args
	if len(args) <= 1 {
		usage()
	}

	args = args[1:]

	switch args[0] {

	case "usage":
		usage()

	case "ast":
		var out string
		var err error

		args = args[1:]

		switch len(args) {

		// piped in
		case 0:
			if !haveStdin() {
				usage()
			}
			out, err = _ast(os.Stdin)
			if err != nil {
				exit(err)
			}

		// explicit file
		case 1:
			if !exists(args[0]) {
				exit(fmt.Errorf("no such file: %v", args[0]))
			}
			if !strings.HasSuffix(args[0], ".pegn") {
				exit(fmt.Errorf("requires .pegn extension"))
			}
			byt, err := ioutil.ReadFile(args[0])
			if err != nil {
				exit(err)
			}
			out, err = _ast(byt)
			if err != nil {
				exit(err)
			}

		default:
			usage()
		}

		fmt.Print(out)

		// end ast

	default:
		var err error
		var clean string

		switch len(args) {

		// piped in
		case 0:
			println("made")
			if !haveStdin() {
				usage()
			}
			clean, err = _print(os.Stdin)
			if err != nil {
				exit(err)
			}

		// explicit file
		case 1:
			if !exists(args[0]) {
				exit(fmt.Errorf("no such file: %v", args[0]))
			}
			if !strings.HasSuffix(args[0], ".pegn") {
				exit(fmt.Errorf("requires .pegn extension"))
			}
			byt, err := ioutil.ReadFile(args[0])
			if err != nil {
				exit(err)
			}
			clean, err = _print(byt)
			if err != nil {
				exit(err)
			}

		default:
			usage()

		} // end case 1

		fmt.Print(clean)
	} // end default

}

func _print(in interface{}) (string, error) {
	var out string
	var err error
	p := new(pegn.Parser)
	err = p.Init(in)
	if err != nil {
		return "", err
	}
	node, err := ast.Grammar(p)
	if err != nil {
		return "", err
	}
	out = node.String()
	return out, nil
}

func _ast(in interface{}) (string, error) {
	var out string
	var err error
	p := new(pegn.Parser)
	err = p.Init(in)
	if err != nil {
		return "", err
	}
	node, err := ast.Grammar(p)
	if err != nil {
		return "", err
	}
	out = node.JSON()
	return out, nil
}

func exit(err ...error) {
	if len(err) > 0 {
		fmt.Println(err)
		os.Exit(1)
	}
	os.Exit(0)
}

func usage() {
	fmt.Fprintf(os.Stderr, "usage: pegn [print|ast] <file>\n")
	os.Exit(1)
}

func haveStdin() bool {
	stat, _ := os.Stdin.Stat()
	if (stat.Mode() & os.ModeCharDevice) == 0 {
		return true
	}
	return false
}

func exists(f string) bool {
	_, err := os.Stat(f)
	if os.IsNotExist(err) {
		return false
	}
	return true
}
