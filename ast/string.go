package ast

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/is"
	"gitlab.com/pegn/pegn-go/nd"
)

// String <- quotable+
func String(p *pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.String, nd.NodeTypes)

	var m *pegn.Mark

	// quotable+
	m, _ = p.Check(is.Min{is.Quotable, 1})
	if m == nil {
		return expected("quotable+", node, p)
	}
	node.Value += p.Parse(m)
	p.Goto(m)
	p.Next()

	return node, nil
}
