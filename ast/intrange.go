package ast

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/nd"
)

// IntRange <-- '[' Integer '-' Integer ']'
func IntRange(p *pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.IntRange, nd.NodeTypes)

	var err error
	var n *pegn.Node
	beg := p.Mark()

	// '['
	_, err = p.Expect('[')
	if err != nil {
		p.Goto(beg)
		return expected("'['", node, p)
	}

	// Integer
	n, err = Integer(p)
	if err != nil {
		p.Goto(beg)
		return expected("Integer", node, p)
	}
	node.AppendChild(n)

	// '-'
	_, err = p.Expect('-')
	if err != nil {
		p.Goto(beg)
		return expected("'-'", node, p)
	}

	// Integer
	n, err = Integer(p)
	if err != nil {
		p.Goto(beg)
		return expected("Integer", node, p)
	}
	node.AppendChild(n)

	// ']'
	_, err = p.Expect(']')
	if err != nil {
		p.Goto(beg)
		return expected("']'", node, p)
	}

	return node, nil
}
