package ast

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/is"
	"gitlab.com/pegn/pegn-go/nd"
)

// TokenId <-- ResTokenId / upper (upper / UNDER Upper)+
func TokenId(p *pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.TokenId, nd.NodeTypes)

	var err error
	var m *pegn.Mark
	var n *pegn.Node

	// ResTokenId
	n, err = ResTokenId(p)
	if err == nil {
		node.AppendChild(n)
		return node, nil
	}

	// upper (upper / UNDER upper)+
	m, err = p.Check(is.Upper, is.Min{is.OneOf{is.Upper, is.Seq{'_', is.Upper}}, 1})
	if m == nil {
		return expected("upper (upper / UNDER upper)+", node, p)
	}
	node.Value = p.Parse(m)
	p.Goto(m)
	p.Next()

	return node, nil
}
