package ast

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/nd"
)

// MinMax <-- '{' Min ',' Max? '}'
func MinMax(p *pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.MinMax, nd.NodeTypes)

	var n *pegn.Node
	var err error
	beg := p.Mark()

	// '{'
	_, err = p.Expect('{')
	if err != nil {
		return expected("'{'", node, p)
	}

	// Min
	n, err = Min(p)
	if err != nil {
		p.Goto(beg)
		return expected("Min", node, p)
	}
	node.AppendChild(n)

	// ','
	_, err = p.Expect(',')
	if err != nil {
		p.Goto(beg)
		return expected("','", node, p)
	}

	// Max
	n, err = Max(p)
	if err == nil {
		node.AppendChild(n)
	}

	// '}'
	_, err = p.Expect('}')
	if err != nil {
		p.Goto(beg)
		return expected("'}'", node, p)
	}

	return node, nil
}
