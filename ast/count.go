package ast

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/is"
	"gitlab.com/pegn/pegn-go/nd"
)

// Count <-- '{' digit+ '}'
func Count(p *pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.Count, nd.NodeTypes)

	var m *pegn.Mark
	var err error

	// '{'
	_, err = p.Expect('{')
	if err != nil {
		return expected("'{'", node, p)
	}

	// digit+
	m, err = p.Check(is.Min{is.Digit, 1})
	if m == nil {
		return expected("digit+", node, p)
	}
	node.Value += p.Parse(m)
	p.Goto(m)
	p.Next()

	// '}'
	_, err = p.Expect('}')
	if err != nil {
		return expected("'}'", node, p)
	}

	return node, nil
}
