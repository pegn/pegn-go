package ast

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/nd"
)

// Range <- AlphaRange / IntRange / UniRange
//        / BinRange / HexRange / OctRange
func Range(p *pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.Range, nd.NodeTypes)

	var err error
	var n *pegn.Node

	// AlphaRange
	n, err = AlphaRange(p)
	if err == nil {
		node.AppendChild(n)
		return node, nil
	}

	// IntRange
	n, err = IntRange(p)
	if err == nil {
		node.AppendChild(n)
		return node, nil
	}

	// UniRange
	n, err = UniRange(p)
	if err == nil {
		node.AppendChild(n)
		return node, nil
	}

	// BinRange
	n, err = BinRange(p)
	if err == nil {
		node.AppendChild(n)
		return node, nil
	}

	// HexRange
	n, err = HexRange(p)
	if err == nil {
		node.AppendChild(n)
		return node, nil
	}

	// OctRange
	n, err = OctRange(p)
	if err == nil {
		node.AppendChild(n)
		return node, nil
	}

	return expected("AlphaRange / IntRange / UniRange / BinRange / HexRange / OctRange", node, p)
}
