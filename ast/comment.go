package ast

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/is"
	"gitlab.com/pegn/pegn-go/nd"
)

func Comment(p *pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.Comment, nd.NodeTypes)

	var err error
	var m *pegn.Mark
	var count int

	count = 0
	for {

		b := p.Mark()

		_, err = EndLine(p)
		if err == nil {
			p.Goto(b)
			break
		}

		m, err = p.Check(is.Unipoint)
		if m == nil {
			p.Goto(b)
			break
		}
		node.Value += p.Parse(m)
		p.Next()

		count++
	}

	if !(count >= 1) {
		return expected("(!EndLine unicode)*", node, p)
	}

	return node, nil
}
