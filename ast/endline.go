package ast

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/nd"
)

// EndLine <-- LF / CR LF / CR
func EndLine(p *pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.EndLine, nd.NodeTypes)

	var m *pegn.Mark

	// LF / CR LF / CR
	for {

		// LF
		m, _ = p.Check('\n')
		if m != nil {
			break
		}

		// CR LF
		m, _ = p.Check("\r\n")
		if m != nil {
			break
		}

		// CR
		m, _ = p.Check('\r')
		if m != nil {
			break
		}

		return expected("LF / CR LF / CR", node, p)
	}

	node.Value += p.Parse(m)
	p.Goto(m)
	p.NewLine()
	p.Next()

	return node, nil
}
