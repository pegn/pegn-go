package ast

import (
	"fmt"

	"gitlab.com/pegn/pegn-go"
)

const (
	LANG          = `PEGN`
	LANG_EXT      = ``
	VERSION_MAJOR = 1
	VERSION_MINOR = 0
	VERSION_PATCH = 0
	VERSION_PRE   = `alpha`
)

func expected(a string, node *pegn.Node, p *pegn.Parser) (*pegn.Node, error) {
	return nil, fmt.Errorf("expected %v at %v", a, p.Mark())
}
