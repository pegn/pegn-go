package ast

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/nd"
)

// Version <- 'v' MajorVer '.' MinorVer '.' PatchVer ('-' PreVer)?
func Version(p *pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.Version, nd.NodeTypes)
	var err error
	var n *pegn.Node

	// 'v'
	_, err = p.Expect('v')
	if err != nil {
		return expected("'v'", node, p)
	}

	// MajorVer
	n, err = MajorVer(p)
	if err != nil {
		return expected("MajorVer", node, p)
	}
	node.AppendChild(n)

	// '.'
	_, err = p.Expect('.')
	if err != nil {
		return expected("'.'", node, p)
	}

	// MinorVer
	n, err = MinorVer(p)
	if err != nil {
		return expected("MinorVer", node, p)
	}
	node.AppendChild(n)

	// '.'
	_, err = p.Expect('.')
	if err != nil {
		return expected("'.'", node, p)
	}

	// PatchVer
	n, err = PatchVer(p)
	if err != nil {
		return expected("PatchVer", node, p)
	}
	node.AppendChild(n)

	for {

		// '-'
		_, err = p.Expect('-')
		if err != nil {
			break
		}

		// PreVer?
		n, err = PreVer(p)
		if err != nil {
			break
		}
		node.AppendChild(n)

		break
	}

	return node, nil
}
