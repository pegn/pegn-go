package ast_test

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
)

func ExampleCheckId() {
	var n *pegn.Node

	// CheckId <-- (upper lower+)+
	p := new(pegn.Parser)

	p.Init("EndLine")
	n, _ = ast.CheckId(p)
	n.Print()

	p.Init("CheckId")
	n, _ = ast.CheckId(p)
	n.Print()

	p.Init("AnotherId")
	n, _ = ast.CheckId(p)
	n.Print()

	// invalid
	p.Init("invalid")
	n, _ = ast.CheckId(p)
	n.Print()

	// Output:
	// ["CheckId", "EndLine"]
	// ["CheckId", "CheckId"]
	// ["CheckId", "AnotherId"]
	// <nil>

}
