package ast_test

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
	
)

func ExampleComEndLine_empty() {
	var n *pegn.Node

	// ComEndLine <- SP* ("# " Comment)? EndLine
	p := new(pegn.Parser)

	// SP\n
	p.Init(" \n")
	n, _ = ast.ComEndLine(p)
	n.Print()

	// Output:
	// ["ComEndLine", [
	//   ["EndLine", "\n"]
	// ]]

}

func ExampleComEndLine_plaintxt() {
	var n *pegn.Node

	// ComEndLine <- SP* ("# " Comment)? EndLine
	p := new(pegn.Parser)

	p.Init("   # Comment here.\n")
	n, _ = ast.ComEndLine(p)
	n.Print()

	// Output:
	// ["ComEndLine", [
	//   ["Comment", "Comment here."],
	//   ["EndLine", "\n"]
	// ]]

}

func ExampleComEndLine_unicode() {
	var n *pegn.Node

	// ComEndLine <- SP* ("# " Comment)? EndLine
	p := new(pegn.Parser)

	p.Init("   # Comment👌 here.\n")
	n, _ = ast.ComEndLine(p)
	n.Print()

	// Output:
	// ["ComEndLine", [
	//   ["Comment", "Comment👌 here."],
	//   ["EndLine", "\n"]
	// ]]

}
