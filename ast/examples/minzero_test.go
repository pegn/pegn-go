package ast_test

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
	
)

func ExampleMinZero() {
	var n *pegn.Node

	// MinZero <-- "*"
	p := new(pegn.Parser)

	// *
	p.Init("*")
	n, _ = ast.MinZero(p)
	n.Print()

	// Output:
	// ["MinZero", "*"]

}
