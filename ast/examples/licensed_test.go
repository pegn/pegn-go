package ast

import (
	"gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
)

func ExampleLicensed() {
	var n *pegn.Node
	p := new(pegn.Parser)
	p.Init("# Licensed under Apache-2, CC-BY-4.0\n")
	n, _ = ast.Licensed(p)
	n.Print()
	// Output:
	// ["Licensed", [
	//   ["Comment", "Apache-2, CC-BY-4.0"],
	//   ["EndLine", "\n"]
	// ]]
}
