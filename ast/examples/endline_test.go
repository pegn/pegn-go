package ast_test

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
	
)

func ExampleEndLine() {

	var n *pegn.Node

	// EndLine <-- LF / CR LF / CR
	p := new(pegn.Parser)

	// LF
	p.Init("\n")
	n, _ = ast.EndLine(p)
	n.Print()

	// CR LF
	p.Init("\r\n")
	n, _ = ast.EndLine(p)
	n.Print()

	// CR
	p.Init("\r")
	n, _ = ast.EndLine(p)
	n.Print()

	// Output:
	// ["EndLine", "\n"]
	// ["EndLine", "\r\n"]
	// ["EndLine", "\r"]

}
