package ast_test

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
	
)

func ExampleInteger() {

	var n *pegn.Node
	p := new(pegn.Parser)

	// 0
	p.Init("0")
	n, _ = ast.Integer(p)
	n.Print()

	// 99
	p.Init("99")
	n, _ = ast.Integer(p)
	n.Print()

	// Output:
	// ["Integer", "0"]
	// ["Integer", "99"]

}
