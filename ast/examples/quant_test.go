package ast_test

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
	
)

func ExampleQuant() {
	var n *pegn.Node

	// Quant  <- Optional / MinZero / MinOne / MinMax / Count
	p := new(pegn.Parser)

	// ?
	p.Init("?")
	n, _ = ast.Quant(p)
	n.Print()

	// +
	p.Init("+")
	n, _ = ast.Quant(p)
	n.Print()

	// *
	p.Init("*")
	n, _ = ast.Quant(p)
	n.Print()

	// {1,3}
	p.Init("{1,3}")
	n, _ = ast.Quant(p)
	n.Print()

	// {5}
	p.Init("{5}")
	n, _ = ast.Quant(p)
	n.Print()

	// Output:
	// ["Quant", [
	//   ["Optional", "?"]
	// ]]
	// ["Quant", [
	//   ["MinOne", "+"]
	// ]]
	// ["Quant", [
	//   ["MinZero", "*"]
	// ]]
	// ["Quant", [
	//   ["MinMax", [
	//     ["Min", "1"],
	//     ["Max", "3"]
	//   ]]
	// ]]
	// ["Quant", [
	//   ["Count", "5"]
	// ]]

}
