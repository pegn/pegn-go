package ast_test

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
	
)

func ExampleSequence() {
	var n *pegn.Node
	p := new(pegn.Parser)
	p.Init("Rule Another*")
	n, _ = ast.Sequence(p)
	n.Print()
	// Output:
	// ["Sequence", [
	//   ["Plain", [
	//     ["CheckId", "Rule"]
	//   ]],
	//   ["Plain", [
	//     ["CheckId", "Another"],
	//     ["MinZero", "*"]
	//   ]]
	// ]]
}
