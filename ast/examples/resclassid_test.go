package ast_test

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
	
)

func ExampleResClassId() {

	var n *pegn.Node

	// ResClassId <-- 'alphanum' / 'alpha' / 'any' / 'bitdig' / 'control'
	//              / 'digit' / 'hexdig' / 'lowerhex' / 'lower' / 'octdig'
	//              / 'punct' / 'quotable' / 'sign' / 'upperhex' / 'upper'
	//              / 'visible' / 'ws' / 'alnum' / 'ascii' / 'blank' / 'cntrl'
	//              / 'graph' / 'print' / 'space' / 'word' / 'xdigit'
	p := new(pegn.Parser)

	p.Init("alphanum")
	n, _ = ast.ResClassId(p)
	n.Print()

	p.Init("any")
	n, _ = ast.ResClassId(p)
	n.Print()

	p.Init("ws")
	n, _ = ast.ResClassId(p)
	n.Print()

	p.Init("lower")
	n, _ = ast.ResClassId(p)
	n.Print()

	p.Init("lowerhex")
	n, _ = ast.ResClassId(p)
	n.Print()

	// Output:
	// ["ResClassId", "alphanum"]
	// ["ResClassId", "any"]
	// ["ResClassId", "ws"]
	// ["ResClassId", "lower"]
	// ["ResClassId", "lowerhex"]

}
