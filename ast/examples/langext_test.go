package ast_test

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
	
)

func ExampleLangExt() {
	var n *pegn.Node

	// LangExt <-- visible{1,20}
	p := new(pegn.Parser)

	// alpha
	p.Init("alpha")
	n, _ = ast.LangExt(p)
	n.Print()

	// Output:
	// ["LangExt", "alpha"]

}
