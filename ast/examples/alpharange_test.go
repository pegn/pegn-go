package ast_test

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
)

func ExampleAlphaRange() {

	var n *pegn.Node

	// AlphaRange <-- "[" Letter "-" Letter "]"
	p := new(pegn.Parser)

	// [a-z]
	p.Init("[a-z]")
	n, _ = ast.AlphaRange(p)
	n.Print()

	// [A-Z]
	p.Init("[A-Z]")
	n, _ = ast.AlphaRange(p)
	n.Print()

	// Output:
	// ["AlphaRange", [
	//   ["Letter", "a"],
	//   ["Letter", "z"]
	// ]]
	// ["AlphaRange", [
	//   ["Letter", "A"],
	//   ["Letter", "Z"]
	// ]]

}
