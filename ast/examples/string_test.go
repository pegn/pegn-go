package ast_test

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
	
)

func ExampleString() {

	var n *pegn.Node

	// String <-- quotable+
	p := new(pegn.Parser)

	// ! a
	p.Init("! a")
	n, _ = ast.String(p)
	n.Print()

	// !
	p.Init("!")
	n, _ = ast.String(p)
	n.Print()

	// <--
	p.Init("<--")
	n, _ = ast.String(p)
	n.Print()

	// string
	p.Init("'string'")
	n, _ = ast.String(p)
	n.Print()

	// Output:
	// ["String", "! a"]
	// ["String", "!"]
	// ["String", "<--"]
	// <nil>

}
