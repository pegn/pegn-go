package ast_test

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
)

func ExampleClassDef_ranges() {
	var n *pegn.Node
	p := new(pegn.Parser)
	p.Init("alpha <- [A-Z] / [a-z]")
	n, _ = ast.ClassDef(p)
	n.Print()
	// Output:
	// ["ClassDef", [
	//   ["ClassId", [
	//     ["ResClassId", "alpha"]
	//   ]],
	//   ["ClassExpr", [
	//     ["AlphaRange", [
	//       ["Letter", "A"],
	//       ["Letter", "Z"]
	//     ]],
	//     ["AlphaRange", [
	//       ["Letter", "a"],
	//       ["Letter", "z"]
	//     ]]
	//   ]]
	// ]]
}

func ExampleClassDef_classes() {
	var n *pegn.Node
	p := new(pegn.Parser)
	p.Init("alnum <- alphanum")
	n, _ = ast.ClassDef(p)
	n.Print()
	// Output:
	// ["ClassDef", [
	//   ["ClassId", [
	//     ["ResClassId", "alnum"]
	//   ]],
	//   ["ClassExpr", [
	//     ["ClassId", [
	//       ["ResClassId", "alphanum"]
	//     ]]
	//   ]]
	// ]]
}
