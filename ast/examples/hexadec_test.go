package ast_test

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
	
)

func ExampleHexadec() {
	var n *pegn.Node

	// Hexadec <-- "x" upperhex+
	p := new(pegn.Parser)

	// x0A
	p.Init("x0A")
	n, _ = ast.Hexadec(p)
	n.Print()

	// xAAA
	p.Init("xAAA")
	n, _ = ast.Hexadec(p)
	n.Print()

	// Output:
	// ["Hexadec", "x0A"]
	// ["Hexadec", "xAAA"]

}
