package ast_test

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
	
)

func ExampleGrammar_least() {
	var n *pegn.Node
	p := new(pegn.Parser)
	p.Init("Some <- thing")
	n, _ = ast.Grammar(p)
	n.Print()
	// Output:
	// ["Grammar", [
	//   ["ScanDef", [
	//     ["CheckId", "Some"],
	//     ["Expression", [
	//       ["Sequence", [
	//         ["Plain", [
	//           ["ClassId", "thing"]
	//         ]]
	//       ]]
	//     ]]
	//   ]]
	// ]]
}

func ExampleGrammar_comment() {
	var n *pegn.Node
	p := new(pegn.Parser)
	p.Init("Some <- thing # some random 👌 comment\n / 'another'{2,20}")
	n, _ = ast.Grammar(p)
	n.Print()
	// Output:
	// ["Grammar", [
	//   ["ScanDef", [
	//     ["CheckId", "Some"],
	//     ["Expression", [
	//       ["Sequence", [
	//         ["Plain", [
	//           ["ClassId", "thing"]
	//         ]]
	//       ]],
	//       ["Comment", "some random 👌 comment"],
	//       ["EndLine", "\n"],
	//       ["Sequence", [
	//         ["Plain", [
	//           ["String", "another"],
	//           ["MinMax", [
	//             ["Min", "2"],
	//             ["Max", "20"]
	//           ]]
	//         ]]
	//       ]]
	//     ]]
	//   ]]
	// ]]
}
