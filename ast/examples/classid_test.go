package ast_test

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
)

func ExampleClassId() {
	var n *pegn.Node

	// ClassId <-- ResClassId / lower (lower / UNDER lower)+
	p := new(pegn.Parser)

	p.Init("alphanum")
	n, _ = ast.ClassId(p)
	n.Print()

	p.Init("any")
	n, _ = ast.ClassId(p)
	n.Print()

	p.Init("classid")
	n, _ = ast.ClassId(p)
	n.Print()

	p.Init("another_id")
	n, _ = ast.ClassId(p)
	n.Print()

	p.Init("_another_id")
	n, _ = ast.ClassId(p)
	n.Print()

	p.Init("an_valid_but_unexpected_")
	n, _ = ast.ClassId(p)
	n.Print()

	// Output:
	// ["ClassId", [
	//   ["ResClassId", "alphanum"]
	// ]]
	// ["ClassId", [
	//   ["ResClassId", "any"]
	// ]]
	// ["ClassId", "classid"]
	// ["ClassId", "another_id"]
	// <nil>
	// ["ClassId", "an_valid_but_unexpected"]

}
