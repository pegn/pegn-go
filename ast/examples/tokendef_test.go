package ast_test

import (
	"fmt"

	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
)

func ExampleTokenDef_rune() {
	var n *pegn.Node
	var err error
	p := new(pegn.Parser)
	p.Init("MYSPACE <- x20\n")
	n, err = ast.TokenDef(p)
	n.Print()
	fmt.Println(err)
	// Output:
	// ["TokenDef", [
	//   ["TokenId", "MYSPACE"],
	//   ["Hexadec", "x20"],
	//   ["EndLine", "\n"]
	// ]]
	// <nil>
}

func ExampleTokenDef_rune_comment() {
	var n *pegn.Node
	var err error
	p := new(pegn.Parser)
	p.Init("MYSPACE <- x20 # with comment\n")
	n, err = ast.TokenDef(p)
	n.Print()
	fmt.Println(err)
	// Output:
	// ["TokenDef", [
	//   ["TokenId", "MYSPACE"],
	//   ["Hexadec", "x20"],
	//   ["Comment", "with comment"],
	//   ["EndLine", "\n"]
	// ]]
	// <nil>
}

func ExampleTokenDef_string() {
	var n *pegn.Node
	var err error
	p := new(pegn.Parser)
	p.Init("MYSPACE <- '<='\n")
	n, err = ast.TokenDef(p)
	n.Print()
	fmt.Println(err)
	// Output:
	// ["TokenDef", [
	//   ["TokenId", "MYSPACE"],
	//   ["String", "<="],
	//   ["EndLine", "\n"]
	// ]]
	// <nil>
}

func ExampleTokenDef_no_tokenids() {
	var n *pegn.Node
	var err error
	p := new(pegn.Parser)
	p.Init("MYEOL <- CR\n")
	n, err = ast.TokenDef(p)
	n.Print()
	fmt.Println(err)
	// Output:
	// <nil>
	// expected TokenVal at U+004D 'M' 1,1-1 (1-1)
}

func ExampleTokenDef_compound() {
	var n *pegn.Node
	var err error
	p := new(pegn.Parser)
	p.Init("MYEOL <- CR LF\n")
	n, err = ast.TokenDef(p)
	n.Print()
	fmt.Println(err)
	// Output:
	// <nil>
	// expected TokenVal at U+004D 'M' 1,1-1 (1-1)
}
