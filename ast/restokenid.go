package ast

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/nd"
)

// ResTokenId <-- 'TAB' / 'CRLF' / 'CR' / 'LF' / 'SP' / 'VT' / 'FF'
//              / 'NOT' / 'BANG' / 'DQ'
//              / 'HASH' / 'DOLLAR' / 'PERCENT' / 'AND' / 'SQ'
//              / 'LPAREN' / 'RPAREN' / 'STAR' / 'PLUS' / 'COMMA'
//              / 'DASH' / 'MINUS' / 'DOT' / 'SLASH' / 'COLON' / 'SEMI'
//              / 'LT' / 'EQ' / 'GT' / 'QUERY' / 'QUESTION' / 'AT'
//              / 'LBRAKT' / 'BKSLASH' / 'RBRAKT' / 'CARET' / 'UNDER'
//              / 'BKTICK' / 'LCURLY' / 'LBRACE' / 'BAR' / 'PIPE'
//              / 'RCURLY' / 'RBRACE' / 'TILDE' / 'UNKNOWN' / 'REPLACE'
//              / 'MAXRUNE' / 'MAXASCII' / 'MAXLATIN' / 'LARROWF'
//              / 'RARROW' / 'LLARROW' / 'RLARROWF' / 'LARROW'
//              / 'LFAT' / 'RARROW' / 'RFAT' / 'WALRUS'
func ResTokenId(p *pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.ResTokenId, nd.NodeTypes)
	var m *pegn.Mark

	for _, v := range []string{
		"TAB", "CRLF", "CR", "LFAT", "SP", "VT", "FF", "NOT", "BANG", "DQ",
		"HASH", "DOLLAR", "PERCENT", "AND", "SQ", "LPAREN",
		"RPAREN", "STAR", "PLUS", "COMMA", "DASH", "MINUS", "DOT",
		"SLASH", "COLON", "SEMI", "LT", "EQ", "GT", "QUERY",
		"QUESTION", "AT", "LBRAKT", "BKSLASH", "RBRAKT", "CARET",
		"UNDER", "BKTICK", "LCURLY", "LBRACE", "BAR", "PIPE",
		"RCURLY", "RBRACE", "TILDE", "UNKNOWN", "REPLACE", "MAXRUNE",
		"MAXASCII", "MAXLATIN", "LARROWF", "RARROWF", "LLARROW", "RLARROW",
		"LARROW", "RARROW", "RFAT", "LF", "WALRUS", "ENDOFDATA",
	} {
		m, _ = p.Check(v)
		if m == nil {
			continue
		}
		node.Value += p.Parse(m)
		p.Goto(m)
		p.Next()

		return node, nil
	}

	return expected("Reserved Token", node, p)
}
