package ast

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/is"
	"gitlab.com/pegn/pegn-go/nd"
)

// Expression <-- Sequence (Spacing '/' SP+ Sequence)*
func Expression(p *pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.Expression, nd.NodeTypes)

	var err error
	var n *pegn.Node

	// Sequence
	n, err = Sequence(p)
	if err != nil {
		return expected("Sequence", node, p)
	}
	node.AppendChild(n)

	for {

		b := p.Mark()

		// Spacing
		sp, err := Spacing(p)
		if err != nil {
			p.Goto(b)
			break
		}

		// '/' SP+
		_, err = p.Expect('/', is.Min{' ', 1})
		if err != nil {
			p.Goto(b)
			break
		}

		// Sequence
		sq, err := Sequence(p)
		if err != nil {
			p.Goto(b)
			break
		}

		node.AdoptFrom(sp)
		node.AppendChild(sq)

	}

	return node, nil
}
