package ast

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/nd"
)

// MinOne <-- '+'
func MinOne(p *pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.MinOne, nd.NodeTypes)
	node.Value = "+"
	var err error

	// '+'
	_, err = p.Expect("+")
	if err != nil {
		return expected("'+'", node, p)
	}

	return node, nil
}
