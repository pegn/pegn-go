package ast

import (
	pegn "gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/is"
	"gitlab.com/pegn/pegn-go/nd"
)

// ClassDef <-- ClassId SP+ '<-' SP+ ClassExpr
func ClassDef(p *pegn.Parser) (*pegn.Node, error) {

	node := pegn.NewNode(nd.ClassDef, nd.NodeTypes)

	var err error
	var n *pegn.Node
	beg := p.Mark()

	// ClassId
	n, err = ClassId(p)
	if err != nil {
		p.Goto(beg)
		return expected("ClassId", node, p)
	}
	node.AppendChild(n)

	// SP+ '<-' SP+
	_, err = p.Expect(is.Min{' ', 1}, "<-", is.Min{' ', 1})
	if err != nil {
		p.Goto(beg)
		return expected("SP+ '<-' SP+", node, p)
	}

	// ClassExpr
	n, err = ClassExpr(p)
	if err != nil {
		p.Goto(beg)
		return expected("ClassExpr", node, p)
	}
	node.AppendChild(n)

	return node, nil
}
