package pegn_test

import (
	"errors"
	"gitlab.com/pegn/pegn-go"
	"gitlab.com/pegn/pegn-go/ast"
	"io/ioutil"
	"net/http"
	"strings"
	"testing"
)

const (
	grammarURL = "https://gitlab.com/pegn/spec/-/raw/master/grammar.pegn"
	eod        = rune(1<<31 - 1)
)

func TestLoadGrammar(t *testing.T) {
	if err := func() error {
		raw, err := downloadFile(grammarURL)
		if err != nil {
			return err
		}

		p := new(pegn.Parser)
		if err := p.Init(raw); err != nil {
			return err
		}

		if _, err := ast.Grammar(p); err != nil {
			return err
		}

		if !p.Done() {
			t.Error("Parser did not read the entire file.")

			m := p.Mark()
			lines := strings.Split(string(raw), "\n")
			t.Errorf("Stopped parsing at:\nline %03d: %s", m.Line, lines[m.Line-1])

			// ast.Grammar(p) should stop after the last definition it recognises.
			// If the parser is not done another ast.Definition(p) should follow.
			n, err := ast.Definition(p)
			t.Error(n, err)
		}

		return nil
	}(); err != nil {
		t.Error(err)
	}
}

func downloadFile(url string) ([]byte, error) {
	response, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	if response.StatusCode != 200 {
		return nil, errors.New("received non 200 response code")
	}

	return ioutil.ReadAll(response.Body)
}
